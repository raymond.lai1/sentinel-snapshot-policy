policy "enforce-ec2-tags-size" {
  source = "./enforce-ec2-tags-size.sentinel"
  enforcement_level = "soft-mandatory"
}

#policy "less-than-100-month" {
#  source = "./costs.sentinel"
#  enforcement_level = "soft-mandatory"
#}
